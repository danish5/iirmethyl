This repository contains the source code for identifying arginine methylation sites in proteins using machine learning.
The folder "folds" is dataset folder that consists of 10-fold cross-validation and independent test data and labels. 
The folder “models” contains best-trained models for all the machine learning classifiers employed. 
The jupyter notebook named “Evaluation” is the procedure of evaluating data along with classifiers parameters. 
The best model could be utilized for the evaluation of data after loading the best models as the evaluation of train and test data in a given file.
To use iIRMethyl for predicting Arginine Methylation sites in the test data. Firstly, download all the models. 
Download test datasets features named as bgh_test_svc.npy in .npy format from folder name folds. 
Now download and run jupyter notebook file name as Evaluation.ipynb and get the final predictions.